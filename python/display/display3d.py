import numpy as np
import os
import segyio
import matplotlib.pyplot as plt
import ipywidgets as widgets
from ipywidgets import interact, interact_manual


def plot3d(a, i0, i1, i2, seismic=True, overlay=None, overlay_alpha=0.3,
            ax_fontsize=18, plt_figsize=(30,30), colorbar=True,
            cmap='gist_ncar', title="", aspect='auto',
            extent=None, axes_names=('i0', 'i1', 'i2'), hspace=0, wspace=0):
    """Plot 3D cube along specified cuts.

    The most important optional arguments are seismic, which should generally be true, and
    extent which describes the ranges on the indices.


    Arguments:
    a -- cube (3D numpy array)
    i0 -- slice along first axis (either an index or or inside the extent provided)
    i1 -- slice along second axis (either an index or or inside the extent provided)
    i2 -- slice along third axis (either an index or or inside the extent provided)
    seismic (default True) -- flips the axes for two images. Used for plotting images
    overlay (default None) -- second cube to highlight specific sections in gray
    overlay_alpha (default 0.3) -- controls how visible overlay is
    ax_fontsize (default 18) -- specifies font size for titles and labels
    plt_figsize (default (30, 30)) -- size of final image
    colorbar (default True) -- places a colorbar on the right
    cmap (default 'gist_ncar') -- colormap to be used in the images
    title (default "") -- title used above all plots
    aspect (default 'auto') -- specified aspec ratio (often depends on extent)
    extent (default None) -- extent of data in format [i0_min, i0_max, i1_min, i1_max, i2_min, i2_max]
    axes_names (default ('i0', 'i1', 'i2')) -- names used for plot axes
    hspace -- vertical space between plots
    wspace -- horizontal space between plots"""

    
    assert a is not None, "bad image array provided"
    shape = np.shape(a)

    if extent is None:
        extent = [0, shape[0] - 1, 0, shape[1] - 1, 0, shape[2] - 1]

    assert i0 >= extent[0] and i0 <= extent[1], f"bad index 0 value of {i0} provided"
    assert i1 >= extent[2] and i1 <= extent[3], f"bad index 1 value of {i1} provided"
    assert i2 >= extent[4] and i2 <= extent[5], f"bad index 2 value of {i2} provided"
    
    
    ## Check for consistency between image and overlay
    if overlay is not None:
        assert np.array_equal(shape, np.shape(overlay))
    
    ## Index ranges
    ll = len(shape)
    n_shape = np.zeros(ll)  # sample number

    for i in np.arange(ll):
        n_shape[i] = shape[i]
    
    index_0 = int((i0 - extent[0]) / (extent[1] - extent[0]) * (shape[0] - 1))
    index_1 = int((i1 - extent[2]) / (extent[3] - extent[2]) * (shape[1] - 1))
    index_2 = int((i2 - extent[4]) / (extent[5] - extent[4]) * (shape[2] - 1))

    fig = plt.figure(figsize=plt_figsize)
    gs = fig.add_gridspec(2, 2, wspace=wspace, hspace=hspace)
    ax_0 = fig.add_subplot(gs[1, 0])
    ax_1 = fig.add_subplot(gs[1, 1], sharey=ax_0)
    ax_2 = fig.add_subplot(gs[0, 0], sharex=ax_0)

    # plot slices
    a0 = a[index_0,:,:]
    a1 = a[:,index_1,:]
    a2 = a[:,:,index_2]
    
    a0 = a0.T
    a1 = a1.T
    vmin = a.min()
    vmax = a.max()
    if seismic:
        extent_0 = [extent[2], extent[3], extent[5], extent[4]] if extent is not None else None
        extent_1 = [extent[0], extent[1], extent[5], extent[4]] if extent is not None else None
    else:
        extent_0 = [extent[2], extent[3], extent[4], extent[5]] if extent is not None else None
        extent_1 = [extent[0], extent[1], extent[4], extent[5]] if extent is not None else None

    extent_2 = [extent[2], extent[3], extent[0], extent[1]] if extent is not None else None

    if seismic:
        map_0 = ax_0.imshow(a0, cmap=cmap, aspect=aspect, vmin=vmin, vmax=vmax, extent=extent_0)
        map_1 = ax_1.imshow(a1, cmap=cmap, aspect=aspect, vmin=vmin, vmax=vmax, extent=extent_1)
    else:
        map_0 = ax_0.imshow(a0, cmap=cmap, aspect=aspect, vmin=vmin, vmax=vmax, extent=extent_0, origin='lower')
        map_1 = ax_1.imshow(a1, cmap=cmap, aspect=aspect, vmin=vmin, vmax=vmax, extent=extent_1, origin='lower')
    map_2 = ax_2.imshow(a2, cmap=cmap, aspect=aspect, vmin=vmin, vmax=vmax, extent=extent_2, origin='lower')

    # plot overlays
    if overlay is not None:
        ax_0.imshow(overlay[index_0,:,:], cmap='gray_r', alpha=overlay_alpha, aspect=aspect, extent=extent_0)
        ax_1.imshow(overlay[:,index_1,], cmap='gray_r', alpha=overlay_alpha, aspect=aspect, extent=extent_1)
        ax_2.imshow(overlay[:,:,index_2], cmap='gray_r', alpha=overlay_alpha,aspect=aspect, extent=extent_2)

    # add slice titles
    ax_0.set_title(f"const {axes_names[0]}={i0}", fontsize=ax_fontsize+2)
    ax_1.set_title(f"const {axes_names[1]}={i1}", fontsize=ax_fontsize+2)
    ax_2.set_title(f"const {axes_names[2]}={i2}", fontsize=ax_fontsize+2)

    ax_0.set_xlabel(f"{axes_names[1]}", fontsize=ax_fontsize)
    ax_0.set_ylabel(f"{axes_names[2]}", fontsize=ax_fontsize)
    ax_1.set_xlabel(f"{axes_names[0]}", fontsize=ax_fontsize)
    ax_1.set_ylabel(f"{axes_names[2]}", fontsize=ax_fontsize)
    ax_2.set_xlabel(f"{axes_names[1]}", fontsize=ax_fontsize)
    ax_2.set_ylabel(f"{axes_names[0]}", fontsize=ax_fontsize)

    fig.suptitle(title, fontsize=50)
    
    ax_0.axhline(y=i2, color='r')
    ax_0.axvline(x=i1, color='r')
    ax_1.axvline(x=i0, color='r')
    ax_1.axhline(y=i2, color='r')
    ax_2.axhline(y=i0, color='r')
    ax_2.axvline(x=i1, color='r')

    if colorbar:
        fig.subplots_adjust(right=0.95)
        cbar_ax = fig.add_axes([0.96, 0.2, 0.02, 0.6])
        fig.colorbar(map_0, cax=cbar_ax)
    return fig

def plot3d_slider(a, **kwargs):
    """Interative version of plot3d

    Must be run in notebooks.

    The most important optional arguments are seismic, which should generally be true, and
    extent which describes the ranges on the indices.

    If axes_names are not specified, then the sliders will appear in order without labels.

    Arguments:
    a -- cube (3D numpy array)
    seismic (default False) -- flips the axes for two images. Used for plotting images
    overlay (default None) -- second cube to highlight specific sections in gray
    overlay_alpha (default 0.3) -- controls how visible overlay is
    ax_fontsize (default 18) -- specifies font size for titles and labels
    plt_figsize (default (30, 30)) -- size of final image
    colorbar (default True) -- places a colorbar on the right
    cmap (default 'gist_ncar') -- colormap to be used in the images
    title (default "") -- title used above all plots
    aspect (default 'auto') -- specified aspec ratio (often depends on extent)
    extent (default None) -- extent of data in format [i0_min, i0_max, i1_min, i1_max, i2_min, i2_max]
    axes_names (default ('i0', 'i1', 'i2')) -- names used for plot axes
    hspace -- vertical space between plots
    wspace -- horizontal space between plots
    """
    
    maxx = np.shape(a)
    print(f"Displaying Volume of shape {maxx}")
    
    if 'extent' not in kwargs:
        print(maxx)
        extent = [0, maxx[0] - 1, 0, maxx[1] - 1, 0, maxx[2] - 1]
        steps = [1, 1, 1]
    else:
        extent = kwargs['extent']
        steps = [(extent[1] - extent[0]) / (maxx[0] - 1),
                (extent[3] - extent[2]) / (maxx[1] - 1),
                (extent[5] - extent[4]) / (maxx[2] - 1)]

    i0 = widgets.FloatSlider(min=extent[0], max=extent[1], step=steps[0],
            value=(extent[0] + extent[1]) / 2, continuous_update=False)
    i1 = widgets.FloatSlider(min=extent[2], max=extent[3], step=steps[1],
            value=(extent[2] + extent[3]) / 2, continuous_update=False)
    i2 = widgets.FloatSlider(min=extent[4], max=extent[5], step=steps[2],
            value=(extent[4] + extent[5]) / 2, continuous_update=False)
    if 'axes_names' not in kwargs:
        ui = widgets.HBox([i0, i1, i2])
        kwargs['axes_names'] = ['i0', 'i1', 'i2']
    else:
        ui = widgets.VBox([
            widgets.HBox([widgets.Label(kwargs['axes_names'][0]), i0]),
            widgets.HBox([widgets.Label(kwargs['axes_names'][1]), i1]),
            widgets.HBox([widgets.Label(kwargs['axes_names'][2]), i2])
            ])
    
    
    def f(i0, i1, i2):
        print(f"Ortho slices at index {i0}, {i1}, {i2}")
        plot3d(a, i0, i1, i2, **kwargs)

    out = widgets.interactive_output(f, {'i0': i0, 'i1': i1, 'i2': i2})
    display(ui, out)


def plot3d_segyio_slider(segy_path, **kwargs):
    """Interactive plot3d_slider from filename
    
    Must be run in notebooks.

    Since this takes a segy filename, it will automatically fill in the necessary
    axes_names and the correct extent.

    The most important optional arguments are seismic, which should generally be true, and
    extent which describes the ranges on the indices.

    If axes_names are not specified, then the sliders will appear in order without labels.

    Arguments:
    overlay (default None) -- second cube to highlight specific sections in gray
    overlay_alpha (default 0.3) -- controls how visible overlay is
    ax_fontsize (default 18) -- specifies font size for titles and labels
    plt_figsize (default (30, 30)) -- size of final image
    colorbar (default True) -- places a colorbar on the right
    cmap (default 'gist_ncar') -- colormap to be used in the images
    title (default "") -- title used above all plots
    aspect (default 'auto') -- specified aspec ratio (often depends on extent)
    hspace -- vertical space between plots
    wspace -- horizontal space between plots"""


    if not os.path.exists(segy_path):
        assert False, f"ERROR: segy file path {segy_path} is invalid"
        
    with segyio.open(segy_path) as f:
        #print(f)
        cube = segyio.tools.cube(f)
        ilines = f.ilines
        xlines = f.xlines
        samples = f.samples
        
    n_ilines = len(ilines)
    n_xlines = len(xlines)
    n_samples = len(samples)
    print(f"Number of inlines: {n_ilines} ranging from {ilines[0]} to {ilines[n_ilines-1]}")
    print(f"Number of xlines: {n_xlines} ranging from {xlines[0]} to {xlines[n_xlines-1]}")
    print(f"Number of samples: {n_samples} ranging from {samples[0]} to {samples[n_samples-1]}")
    
    kwargs['seismic'] = True
    kwargs['axes_names'] = ['ilines', 'xlines', 'time']
    kwargs['extent'] = [ilines[0], ilines[-1], xlines[0], xlines[-1], samples[0], samples[-1]]
    print(f"Extent (ilines, xlines, samples): {kwargs['extent']}")
    
    plot3d_slider(cube, **kwargs)


def generate_gradient(shape, gradient_direction):
    """Generates gradient in a nd-array and a maximum of 1.

    Arguments:
    shape -- tuple of nd-array
    gradient_direction -- non-zero array for the gradient direction
    """
    gradient_direction /= np.linalg.norm(gradient_direction)
    cube = np.zeros(shape)
    tuple_coordinates = np.where(np.ones(cube.shape))
    coordinates = np.c_[tuple_coordinates]
    cube[tuple_coordinates] = coordinates @ gradient_direction
    cube /= cube.max()
    return cube


def spherical_power3(v):
    "Used for fractal generation"
    newv = np.zeros(v.shape)
    newv[:,0] = (3*v[:,2]**2 - v[:,0]**2 - v[:,1]**2)*v[:,0]*(v[:,0]**2 - 3*v[:,1]**2) / (v[:,0]**2 + v[:,1]**2)
    newv[:,1] = (3*v[:,2]**2 - v[:,0]**2 - v[:,1]**2)*v[:,1]*(3*v[:,0]**2 - v[:,1]**2) / (v[:,0]**2 + v[:,1]**2)
    newv[:,2] = (v[:,2]**2 - 3*v[:,0]**2 - 3*v[:,1]**2)*v[:,2]
    newv[np.isnan(newv)] = 0
    return newv

def generate_fractal(shape, steps=20):
    "Generates test mandlebulb of degree 3 using `steps` necessary to push it far from 0."
    cube = np.zeros(shape)
    ix, iy, iz = np.where(np.ones(cube.shape))
    x, y, z = tuple(map(lambda x: 2*(x / x.max() - 0.5), (ix, iy, iz)))
    c = np.c_[x, y, z]
    v = np.zeros((x.shape[0], 3))
    for i in range(steps):
        mask = np.linalg.norm(v, axis=1) < 4
        v[mask,:] = spherical_power3(v[mask,:]) + c[mask,:]

    distance = np.linalg.norm(v, axis=1)
    cube[ix, iy, iz] = 1 / np.maximum(distance, 1)
    return cube
