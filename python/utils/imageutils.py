# -*- coding: utf-8 -*-


import os
from skimage import io


def read_as_gray(image_file):
    """Return a 2 dimensional ndarray numpy array representing the grayscale
    pixel representation of the given image.
    
    Args:
        image_file (str): Path to the image file to be converted. 
            If None (default), logging output will be sent to the terminal
    
    Returns:
        ndArray: a two-dimensional numpy float array 
    """
    
    if not os.path.exists(image_file):
        msg = f"Hold on, file {image_file} does not exist"
        raise IOError(msg)
    if not os.access(image_file, os.R_OK):
        msg = f"WARNING, {image_file} is not readable "
        raise IOError(msg)
    im_gray = io.imread(image_file, as_gray=True)
    return im_gray

