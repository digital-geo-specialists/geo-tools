# -*- coding: utf-8 -*-

import logging


def my_getLogger(log_file=None):
    """Return a logger instance:

    Args:
        log_file (str): Path to the log file to be created. 
            If None (default), logging output will be sent to the terminal

    Returns:
        logger: An instance of the logging.Logger class
    """
    
    log = logging.getLogger()
    for handler in log.handlers[:]:
        log.removeHandler(handler)
    if not log_file:
        log.addHandler(logging.StreamHandler())
    else:
        log.addHandler(logging.FileHandler(log_file))
    return log

log = my_getLogger()
log.setLevel(logging.INFO)

